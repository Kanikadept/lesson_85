import {CREATE_TRACK_SUCCESS, FETCH_TRACKS_SUCCESS} from "../actions/tracksActions";

const initialState = {
    tracks: [],
}

const tracksReducer = (state = initialState, action) => {
        switch (action.type) {
            case FETCH_TRACKS_SUCCESS:
                return {...state, tracks: action.payload};
            case CREATE_TRACK_SUCCESS:
                return {...state, tracks: [action.payload, ...state.tracks]};
            default:
                return state;
        }
}

export default tracksReducer;
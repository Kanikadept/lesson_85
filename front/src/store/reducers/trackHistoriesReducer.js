import {FETCH_TRACK_HISTORY_SUCCESS} from "../actions/historyTracksAction";

const initialState = {
    trackHistory: [],
}

const trackHistoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACK_HISTORY_SUCCESS:
            return {...state, trackHistory: action.payload};
        default:
            return state;
    }
}

export default trackHistoriesReducer;
import {CREATE_ALBUM_SUCCESS, FETCH_ALBUM_SUCCESS, FETCH_ALBUMS_SUCCESS} from "../actions/albumsActions";

const initialState = {
    albums: [],
    currentAlbum: null,
}

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_SUCCESS:
            return {...state, albums: action.payload};
        case FETCH_ALBUM_SUCCESS:
            return {...state, currentAlbum: action.payload};
        case CREATE_ALBUM_SUCCESS:
            return {...state, albums: [action.payload, ...state.albums]};
        default:
            return state;
    }
}

export default albumsReducer;
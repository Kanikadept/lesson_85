import {CREATE_ARTIST_SUCCESS, FETCH_ARTIST_SUCCESS, FETCH_ARTISTS_SUCCESS} from "../actions/artistsActions";

const initialState = {
    artists: [],
    currentArtist: null
}

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artists: action.payload};
        case FETCH_ARTIST_SUCCESS:
            return {...state, currentArtist: action.payload};
        case CREATE_ARTIST_SUCCESS:
            return {...state, artists: [action.payload, ...state.artists]};
        default:
            return state;
    }
}

export default artistsReducer;
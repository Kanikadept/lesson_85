import axiosApi from "../../axiosApi";

export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const fetchAlbumsSuccess = (payload) => ({type: FETCH_ALBUMS_SUCCESS, payload});
export const fetchAlbums = artistId => {
    return async dispatch => {
        try {
            const albumsResponse = await axiosApi.get('/albums', {params: {artist: artistId}});
            dispatch(fetchAlbumsSuccess(albumsResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}

export const FETCH_ALBUM_SUCCESS = 'FETCH_ALBUM_SUCCESS';
export const fetchAlbumSuccess = payload => ({type: FETCH_ALBUM_SUCCESS, payload});
export const fetchAlbum = albumId => {
    return async dispatch => {
        try {
            const albumResponse = await axiosApi.get('/albums/' + albumId);
            dispatch(fetchAlbumSuccess(albumResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}

export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';
export const createAlbumSuccess = payload => ({type: CREATE_ALBUM_SUCCESS, payload});
export const createAlbum = album => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const albumResponse = await axiosApi.post('/albums', album, {headers: {Authorization: token}});
            dispatch(createAlbumSuccess(albumResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}
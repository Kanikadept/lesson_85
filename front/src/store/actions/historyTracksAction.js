import axiosApi from "../../axiosApi";


export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';
export const fetchTrackHistorySuccess = payload => ({type: FETCH_TRACK_HISTORY_SUCCESS, payload});
export const fetchTrackHistory = user => {
    return async dispatch => {
        try {
            const trackHistoryResponse = await axiosApi({
                method: 'GET',
                url: '/trackHistories',
                headers: {
                    Authorization: user.token
                }
            })
           dispatch(fetchTrackHistorySuccess(trackHistoryResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}

export const CREATE_TRACK_HISTORY_SUCCESS = 'CREATE_TRACK_HISTORY_SUCCESS';
export const createTrackHistorySuccess = payload => ({type: CREATE_TRACK_HISTORY_SUCCESS, payload});
export const createTrackHistory = (trackId, user) => {
    return async dispatch => {
        try {
            const trackHistoryResponse = await axiosApi({
                method: 'POST',
                url: '/trackHistories',
                data: {track: trackId},
                headers: {
                    Authorization: user.token
                }
            });
            dispatch(createTrackHistorySuccess(trackHistoryResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}
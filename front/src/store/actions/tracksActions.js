import axiosApi from "../../axiosApi";

export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const fetchTracksSuccess = payload => ({type: FETCH_TRACKS_SUCCESS, payload});
export const fetchTracks = albumId => {
    return async dispatch => {
        try {
            const tracksResponse = await axiosApi.get('/tracks', {params: {album: albumId}});
            dispatch(fetchTracksSuccess(tracksResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}

export const CREATE_TRACK_SUCCESS = 'CREATE_TRACK_SUCCESS';
export const createTrackSuccess = payload => ({type: CREATE_TRACK_SUCCESS, payload});
export const createTrack = track => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const trackResponse = await axiosApi.post('/tracks', track, {headers: {Authorization: token}});
            dispatch(createTrackSuccess(trackResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}
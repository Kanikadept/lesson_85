import axiosApi from "../../axiosApi";

export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const fetchArtistsSuccess = payload => ({type: FETCH_ARTISTS_SUCCESS, payload});
export const fetchArtists = () => {
    return async dispatch => {
        try {
            const artistResponse = await axiosApi.get('/artists');
            dispatch(fetchArtistsSuccess(artistResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}

export const FETCH_ARTIST_SUCCESS = 'FETCH_ARTIST_SUCCESS';
export const fetchArtistSuccess = payload => ({type: FETCH_ARTIST_SUCCESS, payload});
export const fetchArtist = artistId => {
    return async dispatch => {
        try {
            const artistResponse = await axiosApi.get('/artists/' + artistId);
            dispatch(fetchArtistSuccess(artistResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}

export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';
export const createArtistSuccess = payload => ({type: CREATE_ARTIST_SUCCESS, payload});
export const createArtist = artist => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const artistResponse = await axiosApi.post('artists', artist, {headers: {Authorization: token}});
            dispatch(createArtistSuccess(artistResponse.data));
        } catch (err) {
            console.log(err)
        }
    }
}
import {Switch, Route} from 'react-router-dom';
import './App.css';
import ListArtists from "./containers/ListArtists/ListArtists";
import ListAlbums from "./containers/ListAlbums/ListAlbums";
import ListTracks from "./containers/ListTracks/ListTracks";
import Layout from "./components/Layout/Layout";
import Register from "./containers/Register/Register";
import LogIn from "./containers/Login/LogIn";
import ListTrackHistory from "./containers/ListTrackHistory/ListTrackHistory";
import ArtistForm from "./containers/ArtistForm/ArtistForm";
import AlbumForm from "./containers/AlbumForm/AlbumForm";
import TrackForm from "./containers/TrackForm/TrackForm";

const App = () => (
    <div className="App">
        <div className="container">
            <Layout>
                <Switch>
                    <Route path="/" component={ListArtists} exact/>
                    <Route path="/albums/:id" component={ListAlbums} exact/>
                    <Route path="/tracks/:id" component={ListTracks} exact/>
                    <Route path="/register" component={Register} exact/>
                    <Route path="/login" component={LogIn} exact/>
                    <Route path="/trackHistory" component={ListTrackHistory} exact/>
                    <Route path="/addArtist" component={ArtistForm} exact/>
                    <Route path="/addAlbum" component={AlbumForm} exact/>
                    <Route path="/addTrack" component={TrackForm} exact/>
                </Switch>
            </Layout>
        </div>
    </div>
);

export default App;

import React from 'react';
import {NavLink} from "react-router-dom";

import './UserMenu.css';
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../store/actions/usersActions";

const UserMenu = ({user}) => {

    const dispatch = useDispatch();

    const handleLogout = () => {
        dispatch(logoutUser());
    }

    return (
        <div className="user-menu">
            <span>Hello, {user.displayName}</span>
            <div className="user-mennu__image-wrapper"><img src={user.avatarImage} alt=""/></div>
            <NavLink to="/addArtist"><button>Add artist</button></NavLink>
            <NavLink to="/addAlbum"><button>Add album</button></NavLink>
            <NavLink to="/addTrack"><button>Add track</button></NavLink>
            <NavLink to="/trackHistory"><button>Track History</button></NavLink>
            <button onClick={handleLogout}>Logout</button>
        </div>
    );
};

export default UserMenu;
import React from 'react';
import ReactDOM from 'react-dom';
import {Router} from "react-router-dom";
import App from "./App";
import {Provider} from "react-redux";
import history from "./history";
import store from './store/configureStore'

const app = (
    <Provider store={store}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>
)

ReactDOM.render(app,document.getElementById('root'));
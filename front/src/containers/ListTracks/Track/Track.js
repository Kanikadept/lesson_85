import React from 'react';
import './Track.css';

const Track = ({onClick, trackNumber, name, duration}) => {
    return (
        <div className="track" onClick={onClick}>
            <span><b>Number: </b>{trackNumber}</span>
            <span><b>Name: </b>{name}</span>
            <span><b>Duration: </b> {duration}</span>
        </div>
    );
};

export default Track;
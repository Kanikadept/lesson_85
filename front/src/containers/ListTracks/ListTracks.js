import React, {useEffect} from 'react';
import './ListTracks.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchTracks} from "../../store/actions/tracksActions";
import Track from "./Track/Track";
import {fetchAlbum} from "../../store/actions/albumsActions";
import {createTrackHistory} from "../../store/actions/historyTracksAction";

const ListTracks = props => {

    const dispatch = useDispatch();
    const tracks = useSelector(state => state.tracks.tracks);
    const album = useSelector(state => state.albums.currentAlbum);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(fetchTracks(props.match.params.id));
        dispatch(fetchAlbum(props.match.params.id))
    }, [dispatch, props.match.params.id]);


    const handleClick = trackId => {
        dispatch(createTrackHistory(trackId, user));
    }

    return (
        <div className="list-tracks">
            {album && (
                <div className="list-tracks__header">
                    <div className="list-tracks__artist">
                        <b>Artist: </b>{<i>{album.artist.name}</i>}
                    </div>
                    <div className="list-tracks__album">
                        <b>Album: </b>{<i>{album.name}</i>}
                    </div>

                </div>
            )}
            {tracks && tracks.map(track => {
                return <Track onClick={user ? () => handleClick(track._id): undefined} key={track._id} trackNumber={track.number} name={track.name} duration={track.duration}/>
            })}
        </div>
    );
};

export default ListTracks;
import React from 'react';
import './Album.css';

const Album = ({image, name, release}) => {
    return (
        <div className="album">
            <div className="album__image">
                {image && <img className="album__pic" src={'http://localhost:8000/'+image} alt=""/>}
            </div>
            <span className="album__name"><b>Name: </b><i>{name}</i></span>
            <span className="album__release">Release: <i>{release}</i></span>
        </div>
    );
};

export default Album;
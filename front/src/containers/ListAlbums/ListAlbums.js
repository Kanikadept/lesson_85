import React, {useEffect} from 'react';
import './ListAlbums.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchAlbums} from "../../store/actions/albumsActions";
import Album from "./Album/Album";
import {fetchArtist} from "../../store/actions/artistsActions";
import {NavLink} from "react-router-dom";

const ListAlbums = (props) => {

    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.albums);
    const currentArtist = useSelector(state => state.artists.currentArtist);

    useEffect(() => {
        if (props.match.params.id) {
            dispatch(fetchAlbums(props.match.params.id));
            dispatch(fetchArtist(props.match.params.id));
        }
    }, [dispatch, props.match.params.id])

    return (
        <div className="list-albums">
            {currentArtist && (
                <span><b>Artist:</b> <i>{currentArtist.name}</i></span>
            )}
            {albums.map(album => {
                return <NavLink to={'/tracks/' + album._id} key={album._id}><Album image={album.image} name={album.name} release={album.releaseDate}/></NavLink>
            })}
        </div>
    );
};

export default ListAlbums;
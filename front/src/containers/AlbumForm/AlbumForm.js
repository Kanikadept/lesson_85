import React, {useEffect, useState} from 'react';
import FileInput from "../FileInput/FileInput";
import {useDispatch, useSelector} from "react-redux";
import {createAlbum} from "../../store/actions/albumsActions";
import {fetchArtists} from "../../store/actions/artistsActions";

const AlbumForm = props => {

    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);

    const [album, setAlbum] = useState({
        name: '',
        artist: '',
        image: '',
        releaseDate: '',
    });

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    const handleChange = (event) => {
        const {name, value} = event.target;
        setAlbum(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setAlbum(prevSate => ({
            ...prevSate,
            [name]: file
        }));
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(album).forEach(key => {
            formData.append(key, album[key]);
        });
        dispatch(createAlbum(formData));
        props.history.push('/');
    }

    return (
        <form className="log-in" onSubmit={handleSubmit}>
            <span><i>Add new Album</i></span>
            <div className="log-in__row">
                <label><b>Name:</b></label>
                <input type="text" name="name" value={album.name} onChange={handleChange}/>
            </div>
            <div className="log-in__row">
                <label><b>Release date:</b></label>
                <input type="date" name="releaseDate" value={album.releaseDate} onChange={handleChange}/>
            </div>
            <div className="log-in__row">
                <label><b>Artist:</b></label>
                <select name="artist" id="artist" value={album.artist} onChange={handleChange}>
                    {artists.map(artist => {
                        return <option key={artist._id}  value={artist._id}>{artist.name}</option>
                    })}
                </select>
            </div>
            <div className="log-in__row">
                <label><b>Image:</b></label>
                <FileInput name="image" label="Image" onChange={fileChangeHandler}/>
            </div>
            <div className="login__row">
                <button>Add</button>
            </div>
        </form>
    );
};

export default AlbumForm;
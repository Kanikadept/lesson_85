import React, {useEffect} from 'react';
import './ListArtists.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import Artist from "./Artist/Artist";
import {NavLink} from "react-router-dom";

const ListArtists = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch])


    return (
        <div className="list-artists">
            {artists && artists.map(artist => {
                return <NavLink key={artist._id} to={'/albums/' + artist._id}><Artist image={artist.image} name={artist.name}/></NavLink>
            })}
        </div>
    );
};

export default ListArtists;
import React from 'react';
import './Artist.css'

const Artist = ({image, name}) => {
    return (
        <div className="artist">
            <div className="artist__image">
                {image && <img className="artist__pic" src={'http://localhost:8000/'+image} alt=""/>}
            </div>
            <span className="artist__name">{name}</span>
        </div>
    );
};

export default Artist;
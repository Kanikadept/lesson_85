import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import FileInput from "../FileInput/FileInput";
import {createArtist} from "../../store/actions/artistsActions";

const ArtistForm = props => {

    const dispatch = useDispatch();

    const [artist, setArtist] = useState({
        name: '',
        info: '',
        image: ''
    });

    const handleChange = (event) => {
        const {name, value} = event.target;
        setArtist(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setArtist(prevSate => ({
            ...prevSate,
            [name]: file
        }));
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(artist).forEach(key => {
            formData.append(key, artist[key]);
        });
        dispatch(createArtist(formData));
        props.history.push('/');
    }

    return (
        <form className="log-in" onSubmit={handleSubmit}>
            <span><i>Add new Artist</i></span>
            <div className="log-in__row">
                <label><b>Name:</b></label>
                <input type="text" name="name" value={artist.name} onChange={handleChange}/>
            </div>
            <div className="log-in__row">
                <label><b>Info:</b></label>
                <input type="text" name="info" value={artist.info} onChange={handleChange}/>
            </div>
            <div className="log-in__row">
                <label><b>Image:</b></label>
                <FileInput name="image" label="Image" onChange={fileChangeHandler}/>
            </div>
            <div className="login__row">
                <button>Add</button>
            </div>
        </form>
    );
};

export default ArtistForm;
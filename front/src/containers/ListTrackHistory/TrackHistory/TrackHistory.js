import React from 'react';
import './TrackHsitory.css';

const TrackHistory = ({datetime, name, artistName}) => {
    return (
        <div className="track-history">
            <span><i>Track: </i>{name}</span>
            <span><i>Artist: </i>{artistName.name}</span>
            <span>{datetime}</span>
        </div>
    );
};

export default TrackHistory;
import React, {useEffect} from 'react';
import './ListTrackHistory.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchTrackHistory} from "../../store/actions/historyTracksAction";
import TrackHistory from "./TrackHistory/TrackHistory";

const ListTrackHistory = props => {

    const dispatch = useDispatch();
    const trackHistory = useSelector(state => state.trackHistories.trackHistory);
    const user = useSelector(state => state.users.user);
    const artist = useSelector(state => state.artists.currentArtist);

    useEffect(() => {
        dispatch(fetchTrackHistory(user));
    }, [dispatch, user]);

    useEffect(() => {
        if (!user) {
            props.history.push('/login')
        }
    }, [props.history, user])

    return (
        <div className="list-track-history">
            {trackHistory.map(trackHist => {
                return <TrackHistory key={trackHist._id} artistName={artist} name={trackHist.track.name} datetime={trackHist.datetime}/>
            })}
        </div>
    );
};

export default ListTrackHistory;
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createTrack} from "../../store/actions/tracksActions";
import {fetchAlbums} from "../../store/actions/albumsActions";

const TrackForm = props => {

    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.albums);

    const [track, setTrack] = useState({
        number: 0,
        name: '',
        album: '',
        duration: '',
    });

    useEffect(() => {
        dispatch(fetchAlbums());
    }, [dispatch]);

    const handleChange = (event) => {
        const {name, value} = event.target;
        setTrack(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        dispatch(createTrack(track));
        props.history.push('/');
    }

    return (
        <form className="log-in" onSubmit={handleSubmit}>
            <span><i>Add new Track</i></span>
            <div className="log-in__row">
                <label><b>Number:</b></label>
                <input type="text" name="number" value={track.number} onChange={handleChange}/>
            </div>
            <div className="log-in__row">
                <label><b>Name:</b></label>
                <input type="text" name="name" value={track.name} onChange={handleChange}/>
            </div>
            <div className="log-in__row">
                <label><b>Albums:</b></label>
                <select name="album" id="album" value={track.album} onChange={handleChange}>
                    {albums.map(album => {
                        return <option key={album._id}  value={album._id}>{album.name}</option>
                    })}
                </select>
            </div>
            <div className="log-in__row">
                <label><b>Duration:</b></label>
                <input type="text" name="duration" value={track.duration} onChange={handleChange}/>
            </div>

            <div className="login__row">
                <button>Add</button>
            </div>
        </form>
    );
};

export default TrackForm;
const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Artist = require("./models/Artist");
const Album = require("./models/Album");
const Track = require("./models/Track");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for(const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [guf, migos, future] = await Artist.create({
        name: 'Guf',
        info: 'Rap artist',
        image: 'fixtures/GUF.jpg'
    }, {
        name: 'Migos',
        info: 'Rap ,pop  artist',
        image: 'fixtures/MIGOS.jpg'
    }, {
        name: 'Future',
        info: 'Rap, trap, pop artist',
        image: 'fixtures/FUTURE.jpg'
    });

    const [albumGuf1, albumGuf2,
        albumMigos1, albumMigos2,
        albumFuture1, albumFuture2] = await Album.create({
        name: 'Home',
        artist: guf,
        image: 'fixtures/home.jpeg'
    }, {
        name: 'City of roads',
        artist: guf,
        image: 'fixtures/road-city.jpeg'
    }, {
        name: 'Culture II',
        artist: migos,
        image: 'fixtures/Culture_2.jpg'
    }, {
        name: 'Culture',
        artist: migos,
        image: 'fixtures/Culture.jpg'
    }, {
        name: 'The Wizrd',
        artist: future,
        image: 'fixtures/The_Wizrd.jpeg'
    }, {
        name: 'High Off Life',
        artist: future,
        image: 'fixtures/High_Off_Life.jpg'
    });

    await Track.create({
        number: 1,
        name: 'Intro',
        album: albumGuf1,
        duration: '1:21'
    }, {
        number: 2,
        name: 'More staff',
        album: albumGuf1,
        duration: '4:32'
    }, {
        number: 3,
        name: 'Ice Baby',
        album: albumGuf1,
        duration: '2:51'
    }, {
        number: 4,
        name: 'Metropolitan Mail',
        album: albumGuf1,
        duration: '5:11'
    },{
        number: 5,
        name: 'Under the balcony',
        album: albumGuf1,
        duration: '6:21'
    }, {
        number: 6,
        name: 'Tram rails',
        album: albumGuf2,
        duration: '3:21'
    }, {
        number: 7,
        name: 'New year',
        album: albumGuf2,
        duration: '7:11'
    }, {
        number: 8,
        name: 'Wedding',
        album: albumGuf2,
        duration: '2:51'
    }, {
        number: 9,
        name: 'Have questions',
        album: albumGuf2,
        duration: '5:31'
    }, {
        number: 10,
        name: 'gossip',
        album: albumGuf2,
        duration: '7:11'
    }, {
        number: 11,
        name: 'Higher we go',
        album: albumMigos2,
        duration: '1:21'
    }, {
        number: 12,
        name: 'Supastars',
        album: albumMigos2,
        duration: '4:32'
    }, {
        number: 13,
        name: 'Narcos',
        album: albumMigos2,
        duration: '2:51'
    }, {
        number: 14,
        name: 'Auto Pilot',
        album: albumMigos2,
        duration: '5:11'
    },{
        number: 15,
        name: 'Stir Fry',
        album: albumMigos2,
        duration: '6:21'
    }, {
        number: 16,
        name: 'T-shirt',
        album: albumMigos1,
        duration: '3:21'
    }, {
        number: 17,
        name: 'Call Casting',
        album: albumMigos1,
        duration: '7:11'
    }, {
        number: 18,
        name: 'Bad and Boujee',
        album: albumMigos1,
        duration: '2:51'
    }, {
        number: 19,
        name: 'Big on Big',
        album: albumMigos1,
        duration: '5:31'
    }, {
        number: 20,
        name: 'What the price',
        album: albumMigos1,
        duration: '7:11'
    }, {
        number: 21,
        name: 'Never stop',
        album: albumFuture1,
        duration: '1:21'
    }, {
        number: 22,
        name: 'Jumpin on a jet',
        album: albumFuture1,
        duration: '4:32'
    }, {
        number: 23,
        name: 'Rocket ship',
        album: albumFuture1,
        duration: '2:51'
    }, {
        number: 24,
        name: 'Temptation',
        album: albumFuture1,
        duration: '5:11'
    },{
        number: 25,
        name: 'Call the Coroner',
        album: albumFuture1,
        duration: '6:21'
    }, {
        number: 26,
        name: 'Trapped in the sun',
        album: albumFuture2,
        duration: '3:21'
    }, {
        number: 27,
        name: 'HiTek tek',
        album: albumFuture2,
        duration: '7:11'
    }, {
        number: 28,
        name: 'Touch the sky',
        album: albumFuture2,
        duration: '2:51'
    }, {
        number: 29,
        name: 'One of my',
        album: albumFuture2,
        duration: '5:31'
    }, {
        number: 30,
        name: 'Up the river',
        album: albumFuture2,
        duration: '7:11'
    })

    const [user1, user2] =  await User.create({
        username: 'User',
        password: '123',
        token: 'qweqwe',
        displayName: 'Yuri',
        phoneNumber: '12432532432',
        role: 'user'
    }, {
        username: 'Admin',
        password: '321',
        token: 'ewqewq',
        displayName: 'Ivan',
        phoneNumber: '7564543242',
        role: 'admin'
    });

    await mongoose.connection.close();
}

run().catch(console.error);
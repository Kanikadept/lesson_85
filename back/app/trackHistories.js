const express = require('express');

const TrackHistory = require('../models/TrackHistory');
const auth = require("../myMiddleWare/auth");

const router = express.Router();

router.post('/', auth, async (req, res) => {

    try {
        const trackHistory = new TrackHistory(req.body);
        trackHistory.user = req.user;
        trackHistory.datetime = new Date();

        await trackHistory.save();
        return res.send(trackHistory);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.get('/', auth, async (req, res) => {
    try {
        const trackHistory = await TrackHistory.find({user: req.user._id}).populate('track', 'name').sort({datetime: -1});
        return res.send(trackHistory);
    } catch (error) {
        return res.status(400).send(error);
    }
})

module.exports = router;
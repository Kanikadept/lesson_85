const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const path = require("path");
////////////////////////////////////
const Artist = require("../models/Artist");
///////////////////////////////////
const config = require('../config');


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
})

const upload = multer({storage});

router.post('/', upload.single('image'), async (req, res) => {
    try {

        const artistData = req.body;

        if(req.file) {
            artistData.image = 'uploads/' + req.file.filename;
        }

        if (artistData.name.length === 0) {
            res.status(400).send({error: 'name should be filled'});
        }

        const artist = new Artist(artistData);
        await artist.save();
        res.send(artist);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', async (req, res) => {
    try {
        const artistData = await Artist.find();
        res.send(artistData);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const artistData = await Artist.findOne({_id: req.params.id});
        if (artistData) {
            res.send(artistData);
        }
        res.sendStatus(404);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;



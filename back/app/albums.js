const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
////////////////////////////
const Album = require("../models/Album");
const config = require('../config');
const path = require("path");



const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
})

const upload = multer({storage});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        const albumData = req.body;

        if(req.file) {
            albumData.image = req.file.filename;
        }

        if(albumData.artist.length === 0 || albumData.name.length === 0) {
            res.status(400).send({error: 'name and artist should be filled'});
        }

        const album = new Album(albumData);
        const albumResponse = await album.save();
        res.send(albumResponse);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', async (req, res) => {
   try {
       const criteria = {};
       if (req.query.artist) {
           criteria.artist = req.query.artist;
       }

       const albumsData = await Album.find(criteria).sort({releaseDate: 1});
       res.send(albumsData);
   } catch (e) {
       res.status(500).send(e);
   }
});

router.get('/:id', async (req, res) => {
    try {
        const albumsData = await Album.findOne({_id: req.params.id}).populate('artist', 'name info');
        if (albumsData) {
            res.send(albumsData);
        }
        res.sendStatus(404);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;
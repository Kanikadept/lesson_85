const mongoose = require('mongoose');

const AlbumSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist',
        required: true
    },
    image: String,
    releaseDate: {
        type: Date
    },
    published: {
        type: Boolean,
        default: false,
        required: true
    }
});

const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;